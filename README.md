# CLIrics

A small lyrics displayer that uses DBUS events to monitor song changes written in Rust.

Specify a player using the flag -p [player name]

![example](./clirics.GIF)